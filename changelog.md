# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2] - 16/10/2020
### Changed
- Use django 2.2 reverse import
  (dmuyshond)

## [0.1.2] - 16/10/2020
### Changed
- Use json_loads from Passerelle instead of json.loads()
  to prevent strings type errors.
  As it has be done by F.Péters on this repo:
  (https://git.entrouvert.org/passerelle-imio-tax-compute.git/commit/?id=e44768d33645297c77b4005bef0d1aa029c1de56)
  (dmuyshond)

## [0.1.1] - 18/05/2020
### Changed
- Python 3 compatibility
